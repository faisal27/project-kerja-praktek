<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
</head>
<body>
<center><img alt="Logo" src="https://visca-barca.xyz/assets/foto_profil/distarcip.png" style="width:300px"></center>
<h2 style='text-align: center'>Pemberitahuan</h2>
<h3 style='text-align: center'>Dinas Penataan Ruang Kota Bandung</h3>
<p style='text-align: center'>Sehubungan dengan surat pengajuan dari <?= $anggota2->kampus ?> nomor <?= $surat->no_surat_kampus ?> tanggal <?= $tanggal1 ?> perihal permohonan untuk melaksanakan kerja praktek, bahwa nama-nama yang tercantum dibawah ini :</p>
<div align="center">
	<table border="1" cellpadding="5" cellspacing="0">
		<tr>
			<th>No</th>
			<th>Nama</th>
			<th>NPM</th>
			<th>Jurusan</th>
		</tr>
		<?php $no = 1 ?>
		<?php foreach ($anggota as $a) : ?>
			<tr>
				<td><?= $no++ ?></td>
				<td><?= $a->nama_anggota ?></td>
				<td><?= $a->npm ?></td>
				<td><?= $a->jurusan ?></td>
			</tr>
		<?php endforeach; ?>
	</table>
</div>
<p style="text-align: center">Permohonan kerja praktek anda telah kami setujui. Oleh karena itu, segera datang ke kantor kami sebelum <b><?= $tanggal ?></b>. Terima
	kasih</p>
</body>
</html>
